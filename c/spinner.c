/*
ref: https://gitlab.com/teslex/gspinners
*/

#include <stdio.h>
#include <unistd.h>

extern unsigned int sleep(unsigned int __miliseconds) {
	usleep(__miliseconds * 1000);
}

int i = 0;
int framesSize = 10;

char *frames[] = { 
			"⠋",
			"⠙",
			"⠹",
			"⠸",
			"⠼",
			"⠴",
			"⠦",
			"⠧",
			"⠇",
			"⠏" 
};

void printFrame(int frameIndex, char *text) {
	printf("\r%s %s", frames[frameIndex], text);
	fflush(stdout);
}

int main() {
	while (1) {
		if (i == framesSize)
			i = 0;

		printFrame(i, "Spinning..");

		i++;
		sleep(80);
	}
}