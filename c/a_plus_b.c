#include <stdio.h>

int a = 0;
int b = 0;

int main(void) {
	scanf("%d", &a);
	scanf("%d", &b);

	int result = a + b;

	printf("%d + %d = %d\n", a, b, result);
}